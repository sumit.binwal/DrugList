//
//  Study.m
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "Study.h"
#import <QuartzCore/QuartzCore.h>
#import "defines.h"

@implementation Study

@synthesize cards,leftV,tool;
@synthesize term,definition,currentCard,recent;
@synthesize wrong,right,done,privious;

-(void)viewWillAppear:(BOOL)animated{
	[self initParam];
	cardIndex=0;
	
	self.currentCard=[self.cards objectAtIndex:cardIndex];
	
	term.text=[currentCard objectForKey:@"term"];
	definition.text=[currentCard objectForKey:@"definition"];
    
//	[[UIApplication sharedApplication]setStatusBarHidden:YES];
	self.navigationController.navigationBarHidden=YES;
    [self prefersStatusBarHidden];
    self.view.autoresizingMask=YES;
	self.view.autoresizesSubviews=YES;
}
- (BOOL)prefersStatusBarHidden{
    return YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
    [super viewWillDisappear:animated];
  	//[[UIApplication sharedApplication]setStatusBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
	self.tool=[RotateTool new];
	[self initParam];
	[self addSubViews];
    
    [self updateFramesAsPerSize:self.view.frame.size orientation:[UIApplication sharedApplication].statusBarOrientation];
//(400, 255, 120, 60)
    //SH term.frame=CGRectMake(10, 5, 360, 50);
	//SH definition.frame=CGRectMake(10, 65, 360, 150);
}

-(void)initParam{
	
	self.recent=[[RecentStudy alloc]initWithDefaultRecordNum];
	cardIndex=0;
	
	self.currentCard=[self.cards objectAtIndex:cardIndex];
}

-(void)addSubViews{
	self.term=[UILabel new];
	self.definition=[UILabel new];
	//SH term.frame=CGRectMake(10, 5, 310, 50);
    term.frame=CGRectMake(75, 5, windowHeight-statusBarHeight-70, 50);
	term.numberOfLines=0;
    [term setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	term.tag=1;
	term.backgroundColor=[UIColor clearColor];
	//SH definition.frame=CGRectMake(10, 65, 310, 150);
    definition.frame=CGRectMake(10, 70, windowHeight-2*10, 175);
	term.text=[currentCard objectForKey:@"term"];
    [term setFont:[UIFont boldSystemFontOfSize:20]];
    [term setAdjustsFontSizeToFitWidth:YES];
	definition.numberOfLines=0;
    [definition setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	definition.backgroundColor=[UIColor clearColor];
	definition.tag=2;
    definition.hidden=YES;
	CGFloat fontSize=17.0;
	//CGSize size = CGSizeMake(310, 200);
    CGSize size = CGSizeMake(windowHeight-10, 200);
	for (; fontSize >= 0; fontSize--) {
		CGSize labelsize = [[currentCard objectForKey:@"definition"] sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
		if (labelsize.height<definition.frame.size.height) {
			break;
		}
	}
	definition.font = [UIFont systemFontOfSize:fontSize];
	definition.text=[currentCard objectForKey:@"definition"];
	self.view.backgroundColor=[UIColor blackColor];
	//SH leftV=[[UIView alloc]initWithFrame:CGRectMake(5, 20, 390, 220)];
    leftV=[[UIView alloc]initWithFrame:CGRectMake(2, 2, windowHeight-4, windowWidth-70)];
	leftV.clipsToBounds=YES;
	leftV.layer.cornerRadius=2;
    leftV.backgroundColor=[UIColor whiteColor];
    
    UIImageView* line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_line.png"]];
    line.frame = CGRectMake(0, 65,  leftV.frame.size.width - 10, 5);
    [line setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [leftV addSubview:line];
    UIImageView* mark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mark.png"]];
    mark.frame = CGRectMake(5, 5, 60, 60);
    [leftV addSubview:mark];
	
   [self.view addSubview:leftV];
    
    [leftV setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *leftRightViewLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:leftV attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *leftRightViewTrailingConstraint = [NSLayoutConstraint constraintWithItem:leftV attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *leftRightViewTopConstraint = [NSLayoutConstraint constraintWithItem:leftV attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant: 0];
    NSLayoutConstraint *leftRightViewBottomConstraint = [NSLayoutConstraint constraintWithItem:leftV attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self.view addConstraints:@[leftRightViewLeadingConstraint, leftRightViewTrailingConstraint, leftRightViewTopConstraint, leftRightViewBottomConstraint]];
    
	[leftV addSubview:term];
	[leftV addSubview:definition];
	
	self.right=[[UIButton alloc]initWithFrame:CGRectMake(170, 380, 120, 120)];
	[right setTitle:@"" forState:UIControlStateNormal];
	[right setBackgroundImage:[UIImage imageNamed:@"btn_next1.png"] forState:UIControlStateNormal];
	[right addTarget:self action:@selector(studyRight) forControlEvents:UIControlEventTouchUpInside];
    [right setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.view addSubview:right];

	self.wrong=[[UIButton alloc]initWithFrame:CGRectMake(400, 380, 120, 30)];
	[wrong setTitle:@"" forState:UIControlStateNormal];
	[wrong setImage:[UIImage imageNamed:@"btn_try.png"] forState:UIControlStateNormal];
	[wrong addTarget:self action:@selector(studyWrong) forControlEvents:UIControlEventTouchUpInside];
    [wrong setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.view addSubview:wrong];
    
	self.done=[[UIButton alloc]initWithFrame:CGRectMake(35, 380, 120, 30)];
	[done setBackgroundImage:[UIImage imageNamed:@"btn_done1.png"] forState:UIControlStateNormal];
	[done setTitle:@"" forState:UIControlStateNormal];
	[done setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[done addTarget:self action:@selector(doneStudy) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:done];
    
    self.privious=[[UIButton alloc]initWithFrame:CGRectMake(400, 255, 120, 60)];
    [privious setBackgroundImage:[UIImage imageNamed:@"btn_pre.png"] forState:UIControlStateNormal];
    [privious setTitle:@"" forState:UIControlStateNormal];
    [privious setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [privious addTarget:self action:@selector(studyPrevious) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:privious];

}

-(void)studyRight{
	[recent addCard:currentCard];
    [self.recent storeRightRecent];
	[self turnToNext];
	
}
-(void)studyPrevious{
    [recent addCard:currentCard];
    [self.recent storeRightRecent];
    [self turnToBack];
    
}

-(void)studyWrong{
	[recent addCard:currentCard];
	[self.recent storeWrongRecent];
	[self turnToNext];
}
-(void)turnToBack{
    if (cardIndex > 0) {
        self.definition.hidden=YES;
        cardIndex--;
        self.currentCard=[self.cards objectAtIndex:cardIndex];
        term.text=[currentCard objectForKey:@"term"];
        definition.text=[currentCard objectForKey:@"definition"];
        CGRect currentRect=leftV.frame;
        leftV.frame=CGRectMake(35, 320, 350, 240);
        [UIView    beginAnimations:nil context:NULL];
        [UIView    setAnimationDuration:0.4];
        leftV.frame=currentRect;
        [UIView    commitAnimations];
        
    }else {
        [self doneStudy];
    }
}

-(void)turnToNext{
	if (cardIndex<[self.cards count]-1) {
		self.definition.hidden=YES;
		cardIndex++;
		self.currentCard=[self.cards objectAtIndex:cardIndex];
		term.text=[currentCard objectForKey:@"term"];
		definition.text=[currentCard objectForKey:@"definition"];
		CGRect currentRect=leftV.frame;
		leftV.frame=CGRectMake(35, 320, 350, 240);
		[UIView	beginAnimations:nil context:NULL];
		[UIView	setAnimationDuration:0.4];
		leftV.frame=currentRect;
		[UIView	commitAnimations];
		
	}else {
		[self doneStudy];
	}
}

-(void)rotate{
    [tool rotateView:leftV];
	
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	UITouch *touch = [touches anyObject];
    if (touch.view==self.leftV) {
		[self rotate];
	}
  
}
-(void)doneStudy{
	[recent.storeWrong writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"wrong.plist"] atomically:YES];
	[recent.storeRight writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"right.plist"] atomically:YES];

    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)updateFramesAsPerSize:(CGSize)size orientation:(UIInterfaceOrientation)orientation {
    
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        
        CGFloat btnsHeight = 60;
        CGFloat btnsWidth = 120;
        
        CGFloat originX = (size.width - (btnsWidth * 4) - (3 * 5)) * 0.5;
        CGFloat originY = (size.height - (btnsHeight) + 5);

        done.frame = CGRectMake(originX, originY, btnsWidth, btnsHeight);
        right.frame = CGRectMake(5 + done.frame.size.width + done.frame.origin.x, originY, btnsWidth, btnsHeight);
        privious.frame = CGRectMake(5 + right.frame.size.width + right.frame.origin.x, originY, btnsWidth, btnsHeight);
        wrong.frame = CGRectMake(5 + privious.frame.size.width + privious.frame.origin.x, originY, btnsWidth, btnsHeight);
    } else {
        
        CGFloat btnsHeight = 40;
        CGFloat btnsWidth = 70;

        CGFloat originX = (size.width - (btnsWidth * 4) - (3 * 5)) * 0.5;
        CGFloat originY = (size.height - (btnsHeight) - 5);
        
        done.frame = CGRectMake(originX, originY, btnsWidth, btnsHeight);
        right.frame = CGRectMake(5 + done.frame.size.width + done.frame.origin.x, originY, btnsWidth, btnsHeight);
        
        privious.frame = CGRectMake(5 + right.frame.size.width + right.frame.origin.x, originY, btnsWidth, btnsHeight);
        wrong.frame = CGRectMake(5 + privious.frame.size.width + privious.frame.origin.x, originY, btnsWidth, btnsHeight);
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
         [self updateFramesAsPerSize:size orientation:orientation];
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         
     }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end

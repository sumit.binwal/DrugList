//
//  CustomNavigationViewController.m
//  Drug List
//
//  Created by Rohit Sharma on 05/05/19.
//

#import "CustomNavigationViewController.h"
#import "Study.h"

@interface CustomNavigationViewController ()

@end

@implementation CustomNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (BOOL)shouldAutorotate;
{
    if ([[self topViewController] respondsToSelector:@selector(shouldAutorotate)]) {
        return [[self topViewController] shouldAutorotate];
    } else {
        return YES;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([[self topViewController] respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return  [[self topViewController] supportedInterfaceOrientations];
    } else {
        return [super supportedInterfaceOrientations];
    }
}

@end

//
//  Study.h
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RotateTool.h"
#import "RecentStudy.h"
@interface Study : UIViewController {
	NSMutableArray *cards;
	UIView *leftV;
	RotateTool *tool;
	UILabel *term;
	UILabel *definition;
	NSMutableDictionary *currentCard;
	int cardIndex;
	RecentStudy *recent;
	UIButton *wrong;
	UIButton *right;
	UIButton *done;
    UIButton *privious;

}
@property(nonatomic,strong)NSMutableArray *cards;
@property(nonatomic,strong)UIView *leftV;
@property(nonatomic,strong)RotateTool *tool;
@property(nonatomic,strong)UILabel *term;
@property(nonatomic,strong)UILabel *definition;
@property(nonatomic,strong)NSMutableDictionary *currentCard;
@property(nonatomic,strong)RecentStudy *recent;
@property(nonatomic,strong)UIButton *wrong,*right,*done,*privious;
-(void)initParam;
-(void)addSubViews;
-(void)turnToNext;
-(void)doneStudy;
@end

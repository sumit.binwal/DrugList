//
//  Group.h
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Group : NSObject {
	NSString *groupName;
	NSMutableArray *cards;
}

@property(nonatomic,strong)NSString *groupName;
@property(nonatomic,strong)NSMutableArray *cards;

-(id)initWithName:(NSString*)name;

@end

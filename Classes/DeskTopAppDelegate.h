//
//  DeskTopAppDelegate.h
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"
#import "HomeViewController.h"
#import "CustomNavigationViewController.h"

#define allGroups ((DeskTopAppDelegate *)[[UIApplication sharedApplication] delegate]).groups
#define allRightsGroup ((DeskTopAppDelegate *)[[UIApplication sharedApplication] delegate]).rightsGroup
#define allWrongsGroup ((DeskTopAppDelegate *)[[UIApplication sharedApplication] delegate]).wrongsGroup
@class DeskTopViewController;

@interface DeskTopAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    DeskTopViewController *viewController;
	NSMutableArray *gruops;
	NSMutableArray *rightsGroup;
	NSMutableArray *wrongsGroup;
}

@property (nonatomic, strong) HomeViewController *homeCtrl;
@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) CustomNavigationViewController *navigationCtrl;
@property (nonatomic, strong) IBOutlet DeskTopViewController *viewController;
@property (nonatomic, strong) NSMutableArray *groups;
@property (nonatomic, strong) NSMutableArray *rightsGroup;
@property (nonatomic, strong) NSMutableArray *wrongsGroup;
-(void)showCardsOfGroup:(Group*)group;
-(void)studyCards:(NSMutableArray*)cards;
@end


//
//  ChooseStudyCard.m
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "ShowStudyCards.h"
#import "DeskTopAppDelegate.h"
#import "Study.h"
#import "defines.h"

@implementation ShowStudyCards
{
    UIButton *buttonInorder;
    UIButton *buttonShuffle;
    NSInteger *intValStudyTypeIndex;
    NSInteger *intValselectedCourseIndex;
    
}
@synthesize cards,cardsgroup,cardTbl,rightCards,wrongCards,history,pickV;

-(void)viewWillAppear:(BOOL)animated{
   self.navigationItem.title=nil;
	cardType=@"rw";
//	[self pickAllCards];
	cardNum=(int)[self.cardsgroup count];
	[self pickAllCards];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //SH
  //  [self createPickerView];
    
    //SH cardTable is not used
    self.isShuffled = NO;
	self.cardTbl=[[UITableView alloc]initWithFrame:CGRectMake(0, -20, self.view.frame.size.width, 400)];
    [self.cardTbl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	cardTbl.delegate=self;
	cardTbl.dataSource=self;
    intValselectedCourseIndex = 0;
    [cardTbl setAllowsSelection:true];
    
    [self.view addSubview:cardTbl];
    
    [cardTbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *tableTopConstraint = [NSLayoutConstraint constraintWithItem:cardTbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *tableLeadingConstraint = [NSLayoutConstraint constraintWithItem:cardTbl attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *tableTrailingConstraint = [NSLayoutConstraint constraintWithItem:cardTbl attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    [self.view addConstraints:@[tableTopConstraint, tableLeadingConstraint, tableTrailingConstraint]];
    
    UIButton *array=[UIButton new];
    array.frame=CGRectMake(7, cardTbl.frame.size.height + 30, self.view.frame.size.width-20, 35);
    [array setBackgroundImage:[UIImage imageNamed:@"studynow.png"] forState:UIControlStateNormal];
    [array setTitle:@"Study        " forState:UIControlStateNormal];
    [array addTarget:self action:@selector(studyNow) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:array];
    self.view.backgroundColor=[UIColor whiteColor];
    
    CGRect rc = CGRectMake((320-180)/2, array.frame.origin.y + array.frame.size.height + 10, 180, 180);
    if(self.view.frame.size.height>480)
        rc = CGRectMake((self.view.frame.size.width-280)/2, array.frame.origin.y + array.frame.size.height + 10, 280, 280);
    UIImageView* imgStar = [[UIImageView alloc] initWithFrame:rc];
    imgStar.image = [UIImage imageNamed:@"EMSstar.png"];
  //  [self.view addSubview:imgStar];
    
    
    buttonInorder=[UIButton new];
    buttonInorder.frame=CGRectMake(40,  cardTbl.frame.size.height - 10, 50, 35);
    [buttonInorder setImage:[UIImage imageNamed:@"radioSelectedIcon"] forState:UIControlStateNormal];
    [buttonInorder bringSubviewToFront:self.view];
    [buttonInorder setTitle:@"  In Order" forState:UIControlStateNormal];
    [buttonInorder addTarget:self action:@selector(inOrderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [buttonInorder setTintColor:[UIColor blackColor]];
    [buttonInorder setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonInorder sizeToFit];
    intValStudyTypeIndex = 0;
    [self.view addSubview:buttonInorder];
    
    [buttonInorder setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *buttonInorderTopConstraint = [NSLayoutConstraint constraintWithItem:cardTbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:buttonInorder attribute:NSLayoutAttributeTop multiplier:1 constant:-10];
    NSLayoutConstraint *buttonInorderLeadingConstraint = [NSLayoutConstraint constraintWithItem:buttonInorder attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:10];
    NSLayoutConstraint *buttonInorderWidthConstraint = [NSLayoutConstraint constraintWithItem:buttonInorder attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:100];
    NSLayoutConstraint *buttonInorderHeightConstraint = [NSLayoutConstraint constraintWithItem:buttonInorder attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:35];
    [self.view addConstraints:@[buttonInorderTopConstraint, buttonInorderLeadingConstraint, buttonInorderWidthConstraint, buttonInorderHeightConstraint]];
    
    buttonShuffle=[UIButton new];
    buttonShuffle.frame=CGRectMake(90+buttonInorder.frame.size.width,  cardTbl.frame.size.height - 10, 50, 35);
    [buttonShuffle setImage:[UIImage imageNamed:@"radioUnselectedIcon"] forState:UIControlStateNormal];
    [buttonShuffle bringSubviewToFront:self.view];
    [buttonShuffle setTitle:@"  Shuffle" forState:UIControlStateNormal];
    [buttonShuffle addTarget:self action:@selector(shuffelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [buttonShuffle setTintColor:[UIColor blackColor]];
    [buttonShuffle setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonShuffle sizeToFit];
    [self.view addSubview:buttonShuffle];
    
    [buttonShuffle setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *buttonShuffleTopConstraint = [NSLayoutConstraint constraintWithItem:cardTbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:buttonShuffle attribute:NSLayoutAttributeTop multiplier:1 constant:-10];
    NSLayoutConstraint *buttonShuffleTrailingConstraint = [NSLayoutConstraint constraintWithItem:buttonShuffle attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10];
    NSLayoutConstraint *buttonShuffleWidthConstraint = [NSLayoutConstraint constraintWithItem:buttonShuffle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:100];
    NSLayoutConstraint *buttonShuffleHeightConstraint = [NSLayoutConstraint constraintWithItem:buttonShuffle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:35];
    [self.view addConstraints:@[buttonShuffleTopConstraint, buttonShuffleTrailingConstraint, buttonShuffleWidthConstraint, buttonShuffleHeightConstraint]];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [array setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *arrayTopConstraint = [NSLayoutConstraint constraintWithItem:buttonShuffle attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:array attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *arrayLeadingConstraint = [NSLayoutConstraint constraintWithItem:array attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:10];
    NSLayoutConstraint *arrayTrailingConstraint = [NSLayoutConstraint constraintWithItem:array attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10];
    NSLayoutConstraint *arrayBottomConstraint = [NSLayoutConstraint constraintWithItem:array attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-10];
    NSLayoutConstraint *arrayHeightConstraint = [NSLayoutConstraint constraintWithItem:array attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:35];
    [self.view addConstraints:@[arrayTopConstraint, arrayLeadingConstraint, arrayTrailingConstraint, arrayHeightConstraint, arrayBottomConstraint]];
}

//SH
- (void)createPickerView
{
    self.pickV=[[UIPickerView alloc]initWithFrame:CGRectZero];
    pickV.frame=CGRectMake(0, statusBarHeight + navigationBarHeight + 10, self.view.frame.size.width, 162);
    pickV.showsSelectionIndicator = YES;
    pickV.dataSource=self;
    pickV.delegate=self;
    [pickV setBackgroundColor:[UIColor clearColor]];
    pickV.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.view addSubview:pickV];
}

//-(void)inOrderButtonClicked
//{
// //radioSelectedIcon
//    [buttonInorder setImage:[UIImage imageNamed:@"radioSelectedIcon"] forState:UIControlStateNormal];
//    [buttonShuffle setImage:[UIImage imageNamed:@"radioUnselectedIcon"] forState:UIControlStateNormal];
//
//
//}
-(IBAction)inOrderButtonClicked:(UIButton*)sender
{
   // self.isShuffled = NO;
    [sender setImage:[UIImage imageNamed:@"radioSelectedIcon"] forState:UIControlStateNormal];
    [buttonShuffle setImage:[UIImage imageNamed:@"radioUnselectedIcon"] forState:UIControlStateNormal];
    intValStudyTypeIndex = 0;
  //  [self.cardTbl reloadData];
}

-(IBAction)shuffelBtnClicked:(UIButton*)sender
{
 //   self.isShuffled = YES;
//    [buttonShuffle setImage:[UIImage imageNamed:@"radioSelectedIcon"] forState:UIControlStateNormal];
    intValStudyTypeIndex = 1;
    [buttonInorder setImage:[UIImage imageNamed:@"radioUnselectedIcon"] forState:UIControlStateNormal];
    [sender setImage:[UIImage imageNamed:@"radioSelectedIcon"] forState:UIControlStateNormal];
  //  self.shuffledCardsgroup = [self shuffle:self.cardsgroup];
  //  [self.cardTbl reloadData];
}

-(void)studyNow{
    [self checkStudyType];
//    DeskTopAppDelegate* delegate=(DeskTopAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Study *study=[[Study alloc]init];
	if ([cardType isEqualToString:@"rw"]) {
//        [delegate studyCards:rightCards];
        study.cards=self.rightCards;
	}else if([cardType isEqualToString:@"w"]){
		if([wrongCards count]>0)
            study.cards=self.wrongCards;
	}
//    if (intValselectedCourseIndex != 1)
//    {
    NSLog(@"%lu",(unsigned long)[wrongCards count]);
    if (intValselectedCourseIndex == 1 && [wrongCards count] > 0)
    {
    [self.navigationController pushViewController:study animated:YES];
    }
    else if (intValselectedCourseIndex != 1)
    {
        [self.navigationController pushViewController:study animated:YES];

    }
    
//    }
    
    
}			 
			 
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	 return 45;
 }
 
 -(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
	 
	 return 1;
 }
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
    return nil;
}
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

//    if ([cardType isEqualToString:@"w"]) {
//
//        return [wrongCards count];
//    }else {
//        return [rightCards count];
//    }
     
         return self.cardsgroup.count+2;
     

 }
 
 -(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
	 
	 static NSString *ShowStudyTableIdentifier = @"ShowStudyTableIdentifier";
//     NSInteger index=indexPath.row;
//     if ([cardType isEqualToString:@"w"])
//         card=[self.wrongCards objectAtIndex:index];
//     else
//         card=[self.rightCards objectAtIndex:index];
	 UITableViewCell *cell;
	 cell= [tableView dequeueReusableCellWithIdentifier:ShowStudyTableIdentifier];
	 
	 if (!cell) {
		 //cell=[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:nil];
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ShowStudyTableIdentifier];

		 //cell.textColor=[UIColor whiteColor];
	 }
     if (indexPath.row == 0)
     {
     cell.textLabel.text=@"All";
     }
     else if (indexPath.row == 1)
     {
     cell.textLabel.text=@"Incorrect Pile";
     }
     else
     {

             cell.textLabel.text=[NSString stringWithFormat:@"%@",[[self.cardsgroup objectAtIndex:indexPath.row-2] groupName]];

     }
     
     
         if (intValselectedCourseIndex == [indexPath row])
         {
         cell.accessoryType = UITableViewCellAccessoryCheckmark;
         }
         else
         {
         cell.accessoryType = UITableViewCellAccessoryNone;
         }
     
	 return cell;
 }
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	// [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
     intValselectedCourseIndex = [indexPath row];
     
     [tableView reloadData];
     
//     UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//
//     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     
 }
//pickview delegate datasource

- (NSInteger )numberOfComponentsInPickerView:(UIPickerView *)pickerView
{  
	return 2;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	NSString *title;
	
	
	if (component==0) {
		NSInteger i = [self.cardsgroup count];
        if (self.isShuffled) {
            i = [self.shuffledCardsgroup count];
        }
		for (int j=0; j<=i; j++) {
			if (row==0) {
				title=@"-All";
			}else if (row==1) {
				title=@"-Incorrect Pile";
			} else if (row==j+2) {
				title=[NSString stringWithFormat:@"%@",[[self.cardsgroup objectAtIndex:j] groupName]];
                if (self.isShuffled) {
                    title = [NSString stringWithFormat:@"%@",[[self.shuffledCardsgroup objectAtIndex:j] groupName]];
                }
			}
		}
		
	}else if (component==1) {
		if (row==0) {
			title=@"In Order";
		}else if (row==1) {
			title=@"Shuffle";
		}
	}
	UILabel *tView = [[UILabel alloc] initWithFrame:CGRectMake(0.0f,0.0f, [pickV rowSizeForComponent:component].width, [pickV rowSizeForComponent:component].height)];
	[tView setBackgroundColor:[UIColor clearColor]];
	[tView setText:title];
	//[tView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:24]];
	tView.adjustsFontSizeToFitWidth=YES;
	[tView setTextAlignment:NSTextAlignmentCenter];
	  return tView;
}
- (NSInteger )pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger )component
{   
	NSInteger num;
	

	if (component==0) {
		num=[self.cardsgroup count]+2;
	}else if (component==1) {
		num=2;
	}
	

	
	return num;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger )row forComponent:(NSInteger )component
{ 
	
	NSString *title;
	
	
	if (component==0) {
		NSInteger i =[self.cardsgroup count];
		for (int j=0; j<=i; j++) {
			if (row==0) {
				title=@"-All";
			}else if (row==1) {
				title=@"-Incorrect Pile";
			} else if (row==j+2) {
				title=[NSString stringWithFormat:@"%@",[[self.cardsgroup objectAtIndex:j] groupName]];
			}
		}
		
	}else if (component==1) {
		if (row==0) {
			title=@"In Order";
		}else if (row==1) {
			title=@"Shuffle";
		}
	}		
	return title;
}

- (void )pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger )row inComponent:(NSInteger )component
{   
    
//    if (component==0) {
//        int i =(int)[self.cardsgroup count];
//        for (int j=0; j<=i; j++) {
//            if (row==0) {
//                cardNum=i;
//                cardType=@"rw";
//                [self.wrongCards removeAllObjects];
//                [self.cardTbl reloadData];
//            }else if (row==1) {
//                cardNum=0;
//                cardType=@"w";
//            } else if (row==j+2) {
//                cardNum=j;
//                cardType=@"rw";
//                [self.wrongCards removeAllObjects];
//                [self.cardTbl reloadData];
//            }
//        }
//        
//    }else if (component==1) {
//        if (row==0) {
//            isShuffled=NO;
//        }else if (row == 1){
//            isShuffled=YES;
//        }
//        
//    }
//    if (cardNum==[self.cardsgroup count]) {
//        [self pickAllCards];
//    }else {
//        [self pickCardsNum:cardNum];
//    }

}
- (void)checkStudyType{
    NSInteger row1 = intValselectedCourseIndex;
    NSInteger row2 = intValStudyTypeIndex;
    
    
        int i =(int)[self.cardsgroup count];
        for (int j=0; j<=i; j++) {
            if (row1==0) {
                cardNum=i;
                cardType=@"rw";
                [self.wrongCards removeAllObjects];
                [self.cardTbl reloadData];
            }else if (row1==1) {
                cardNum=0;
                cardType=@"w";
                [self pickWrongCards];
            } else if (row1==j+2) {
                cardNum=j;
                cardType=@"rw";
                [self.wrongCards removeAllObjects];
                [self.cardTbl reloadData];
            }
        }
        
        if (row2==0) {
            self.isShuffled=NO;
        }else if (row2 == 1){
            self.isShuffled=YES;
            
        }
        if (cardNum==[self.cardsgroup count]) {
            [self pickAllCards];
        }else {
            [self pickCardsNum:cardNum];
        }
        
    
//
//    int i =(int)[self.cardsgroup count];
//    for (int j=0; j<=i; j++) {
//        if (row1==0) {
//            cardNum=i;
//            cardType=@"rw";
//            [self.wrongCards removeAllObjects];
//            [self.cardTbl reloadData];
//        }else if (row1==1) {
//            cardNum=0;
//            cardType=@"w";
//        } else if (row1==j+2) {
//            cardNum=j;
//            cardType=@"rw";
//            [self.wrongCards removeAllObjects];
//            [self.cardTbl reloadData];
//        }
//    }
//    if (row2==0) {
//        _isShuffled=NO;
//    }else if (row2 == 1){
//        _isShuffled=YES;
//
//    }
//    if (cardNum==[self.cardsgroup count]) {
//        [self pickAllCards];
//    }else {
//        [self pickCardsNum:cardNum];
//    }

}
- (CGFloat )pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger )component
{    
	CGFloat w;

	if (component==0) {
		w=195.0f;
	}else if (component==1) {
		w=105.0f;
	}
	return w;
}
-(void)pickAllCards
{
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:0];
    NSInteger i = [self.cardsgroup count];
    for (int j=0; j<i; j++) {
        [array addObjectsFromArray:[[self.cardsgroup objectAtIndex:j] cards]];
    }
    if (!_isShuffled) {
        self.rightCards=[[NSMutableArray alloc] initWithArray:array];
    }else {
        self.rightCards=[self randArray:array];
    }
}
-(void)pickCardsNum:(int)num{
    
    if ([cardType isEqual:@"rw"]) {
        NSMutableArray *array=[[NSMutableArray alloc] initWithArray:[[self.cardsgroup objectAtIndex:num] cards]];
        if (self.isShuffled) {
            self.rightCards=[self randArray:array];
        }else {
            self.rightCards=[[NSMutableArray alloc] initWithArray:array];
        }
        
    }
    else if([cardType isEqual:@"w"]){
        [self pickWrongCards];
    }
    
    [self.cardTbl reloadData];
}
-(void)pickWrongCards{
    
    NSMutableArray *setWrongCards = allWrongsGroup
    ;
    self.history=[RecentStudy new];
    if (self.isShuffled) {
        self.wrongCards=[self randArray:setWrongCards];
    }else {
        self.wrongCards = [[NSMutableArray alloc] initWithArray:setWrongCards];
    }
    
    
    
    [self.cardTbl reloadData];
}
- (NSMutableArray *)randArray:(NSMutableArray *)ary{
    NSMutableArray *tmpAry = [NSMutableArray arrayWithArray:ary];
    NSUInteger count = [ary count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger nElements = count - i;
        // Seed the random number generator
        srandom((unsigned int)time(NULL));
        NSInteger n = (random() % nElements) + i;
        [tmpAry exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    //NSLog(@"chenggong");
    return [[NSMutableArray alloc] initWithArray:tmpAry];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (NSArray *)shuffle:(NSMutableArray *)arr
{
    NSMutableArray *mutArr = [NSMutableArray arrayWithArray:arr];
    NSUInteger count = [mutArr count];
    if (count > 1) {
        for (NSUInteger i = 0; i < count - 1; ++i) {
            NSInteger remainingCount = count - i;
            NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
            [mutArr exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
        }
    }
    return  [NSArray arrayWithArray:mutArr];
}

@end

//
//  StudyResult.h
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentStudy.h"
@interface StudyResult : UIView<UITableViewDelegate,UITableViewDataSource> {
    UIView *leftRightView;
	UIButton *left;
	UIButton *right;
	RecentStudy *rstSdy;
	NSMutableArray *rightRecorts;
	NSMutableArray *wrongRecorts;
	UITableView *tableResult;
	NSMutableDictionary *card;
	NSMutableArray *currentArray;
}
@property(nonatomic,strong)RecentStudy *rstSdy;
@property(nonatomic,strong)NSMutableArray *rightRecorts,*wrongRecorts;
@property(nonatomic,strong)UITableView *tableResult;
@property(nonatomic,strong)NSMutableDictionary *card;
@property(nonatomic,strong) NSLayoutConstraint *leftHeightConstraint;
@property(nonatomic,strong) NSLayoutConstraint *rightHeightConstraint;
-(void)readRecentRecords;
-(void)showRightRecords;
-(void)addSubviews;
@end
